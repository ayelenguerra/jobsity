<?php

namespace Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Foundation\Testing\RefreshDatabase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__ . '/../bootstrap/app.php';

        $app->make(Kernel::class)->bootstrap();

        return $app;
    }

    public function setUp(): void
    {
        parent::setUp();

        $this->createApplication();

        Artisan::call('migrate:fresh');
        Artisan::call('db:seed');
    }

    public function tearDown(): void
    {
        parent::tearDown();
    }

    protected function getHeader()
    {
        $header = ['account-id' => $this->_accountId, 'Authorization' => $this->getAccessToken()];

        return $header;
    }
}

