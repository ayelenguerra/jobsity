<?php

namespace Tests\Unit;

use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    private $post_structure = ['title', 'content', 'author_id'];

    /**
     * PostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testCreatePost(): void
    {
        $user = factory(User::class)->create();
        Auth::login($user);
        $data = ['title' => 'test_title', 'content' => 'test_content'];
        $response = $this->actingAs($user)->json('POST', '/admin/post', $data);

        $response
            ->assertStatus(201)
            ->assertJsonStructure($this->post_structure)->assertJsonFragment($data);
    }

    public function testUpdatePost(): void
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->create();
        Auth::login($user);

        $data = [
            'title' => $post->title . ' (updated)',
            'content' => $post->content . ' (updated)',
        ];
        $response = $this->actingAs($user)->json('PATCH', '/admin/post/' . $post->id, $data);

        $response
            ->assertStatus(200)
            ->assertJsonStructure($this->post_structure)
            ->assertJsonFragment($data);
    }

    public function testDeletePost(): void
    {
        $user = factory(User::class)->create();
        $post = factory(Post::class)->create();
        Auth::login($user);

        $response = $this->actingAs($user)->json('DELETE', '/admin/post/' . $post->id);
        $response->assertStatus(302);
    }
}
