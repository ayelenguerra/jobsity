<?php

namespace Tests\Unit;

use App\Models\Tweet;
use App\Models\User;
use Illuminate\Support\Facades\Log;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

class TwitterTest extends TestCase
{
    use RefreshDatabase;
    /**
     * PostTest constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function testGetTweets(): void
    {
        $user = factory(User::class)->create();
        $response = $this->json('GET', '/api/user/'.$user->id.'/hidden_tweets');

        $response
            ->assertStatus(200)
            ->assertJsonStructure();
    }

    public function testShowTweet(): void
    {
        $user = factory(User::class)->create();
        $tweet_id = '437386738563';
        Tweet::create([
            'user_id' => $user->id,
            'internal_id' => $tweet_id
        ]);
        Auth::login($user);
        $response = $this->actingAs($user)->json('GET', '/api/user/'.$user->id.'/show_tweet/' . $tweet_id);

        Log::info($response->content());
        $response
            ->assertStatus(200)
            ->assertSeeText('success');
    }


    public function testHideTweet(): void
    {
        $user = factory(User::class)->create();
        $tweet_id = '437386738563';
        Tweet::create([
            'user_id' => $user->id,
            'internal_id' => $tweet_id
        ]);
        Auth::login($user);
        $response = $this->actingAs($user)->json('GET', '/api/user/'.$user->id.'/hide_tweet/' . $tweet_id);

        $response
            ->assertStatus(200)
            ->assertSeeText('success');
    }
}
