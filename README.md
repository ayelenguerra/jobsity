# Jobsity challenge

## Setting

To generate the dependencies run: 
```shell
composer install
```      

To set the application key: 
```shell
php artisan key:generate  
```      

Then copy `.env.example` into `.env` to have the environment variables set.

### Database:

DB creation script: 
```shell
create schema jobsity 
```      

To create the tables and add dummy data run into the project folder: 
```shell
php artisan migrate && php artisan db:seed 
```      

A database backup was uploaded to the Bitbucket repository (https://bitbucket.org/ayelenguerra/jobsity/downloads/)

Run server:

 
```shell
php artisan serve
```      


### css/js:

Generate css/jss files (into the project folder):

 
```shell
npm run dev
```      


## Project Information:

The site is responsive and has an authentication system that allows updating user account data, login and register. 

Also s dashboard was developed, which include a CRUD for the posts and access to the user profile update.


#### User credentials

username: john.doe

password: 123123123       


#### Tests

The developed tests can be executed with:

```shell
./vendor/bin/phpunit 
```                  

### Virtual hosts setting

#### httpd.conf setting

Open the  httpd.conf file with an editor:

```shell
sudo nano /usr/local/etc/httpd/httpd.conf
```
                                                                 

Check that is listen to the port 80

```bash
	Listen 80
```

Uncomment the next lines to enable the modules:

```bash
	LoadModule deflate_module libexec/apache2/mod_deflate.so
	LoadModule expires_module libexec/apache2/mod_expires.so
	LoadModule rewrite_module libexec/apache2/mod_rewrite.so
	LoadModule php7_module libexec/apache2/libphp7.so 
	LoadModule userdir_module libexec/apache2/mod_userdir.so 
	LoadModule vhost_alias_module libexec/apache2/mod_vhost_alias.so 
	LoadModule include_module libexec/apache2/mod_include.so  
```

Uncomment the next line to include the file:

```bash
Include /usr/local/etc/httpd/extra/httpd-vhosts.conf
```
	
To enable PHP in Apache add the following:

```bash
<FilesMatch \.php$>
	SetHandler application/x-httpd-php
</FilesMatch>
```

And then check DirectoryIndex includes index.php

```bash
DirectoryIndex index.php index.html
```
Save and close de file.

#### httpd-vhosts.conf setting

Open the  httpd.conf file with an editor:

```shell
sudo nano  /usr/local/etc/httpd/extra/httpd-vhosts.conf
```

Add the virtual host configuration:

```bash
<VirtualHost *:80>
    ServerAdmin web@jobsity.com 
    DocumentRoot "/var/www/jobsitychallenge/ayelen/public"
    <Directory "/var/www/jobsitychallenge/ayelen/public">
        Options All  
        AllowOverride all
        Order allow,deny
        Allow from all
    </Directory>
    ServerName ayelen.jobsitychallenge.com
    ErrorLog "/usr/local/var/log/httpd/ayelen.jobsitychallenge.com-error_log" 
    CustomLog "/usr/local/var/log/httpd/ayelen.jobsitychallenge.com-access_log" common 
</VirtualHost>
```

Save and close de file.


#### Hosts setting

Open the hosts file with an editor:

```shell
sudo nano  /etc/hosts
```

Add the next line:

```bash
127.0.0.1       ayelen.jobsitychallenge.com
```

Save and close de file.

Restart Apache:

```shell
apachectl restart
```


#### Laravel setting

Change the project owner

```shell
sudo chown -R $USER:_www /var/www/jobsitychallenge/ayelen
```

Find every file "-type f" , every directory "-type d" execute the chmod command to 664 and 775 for both.

```shell
sudo find /var/www/jobsitychallenge/ayelen -type f -exec chmod 664 {} \; 
sudo find /var/www/jobsitychallenge/ayelen -type d -exec chmod 775 {} \;
```

Change the owner recursively across the storage and bootstrap/cache folders and gives web server read, write and execute permissions 

```shell
sudo chgrp -R _www /var/www/jobsitychallenge/ayelen/storage /var/www/jobsitychallenge/ayelen/bootstrap/cache
sudo chmod -R ug+rwx /var/www/jobsitychallenge/ayelen/storage /var/www/jobsitychallenge/ayelen/bootstrap/cache
```                       
