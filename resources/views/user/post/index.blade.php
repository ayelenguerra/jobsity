@extends('layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

    <h1>{{$author->username}}</h1>
    <input type="hidden" id="twitter_username" value="{{$author->twitter_username}}">
    <input type="hidden" id="user_id" value="{{$author->id}}">

    <div class="row mb-4">
        <div class="col-md-9">
            @foreach($posts as $post)
                <div class="row mb-4">
                    <div class="col">
                        <div class="card">
                            <div class="card-header">
                                <a href="{{ route('frontend.post.show', $post) }}" data-toggle="tooltip"
                                   data-placement="top"
                                   title="@lang('buttons.general.crud.view')">
                                    {{ $post->title }}
                                    @if (Auth::check() && Auth::user()->id == $post->author->id)

                                        <div class="btn-group float-right" role="group"
                                             aria-label="@lang('labels.backend.access.posts.post_actions')">
                                            <a href="{{ route('admin.post.edit', $post) }}" data-toggle="tooltip"
                                               data-placement="top"
                                               title="@lang('buttons.general.crud.edit')" class="btn btn-primary">
                                                <i class="fas fa-edit"></i>
                                            </a>
                                        </div>

                                    @endif
                                </a>
                            </div>
                            <div class="card-body">
                                {{ \Illuminate\Support\Str::words($post->content, 50, ' ...') }}
                                <a href="{{ route('frontend.post.show', $post) }}" data-toggle="tooltip"
                                   data-placement="top">
                                    (see more)
                                </a>
                            </div>
                            <div class="card-footer">
                                <div class="float-right">
                                    <a href="{{ route('frontend.user.post.show', $post->author) }}"
                                       data-toggle="tooltip"
                                       data-placement="top">
                                        {{ $post->author->username }}
                                    </a>
                                </div>
                            </div>
                        </div><!--card-->
                    </div><!--col-->
                </div><!--row-->
            @endforeach
            <div class="text-xs-center">
                {{ $posts->links() }}

            </div>
        </div><!--row-->

        <div class="col-md-3">
            <div id="tweets">
            </div>
        </div><!--row-->
    </div><!--row-->

@endsection
@push('after-scripts')
    <script type="text/javascript" src="{{ URL::asset('js/TweetJs.js') }}"></script>
    <script>

        $(document).ready(function () {
            getHiddenTweets();
        });

        var hidden_tweets = [];
        var logged_in = "{{{ (Auth::user()) ? true : false }}}";
        var auth_user = "{{{ (Auth::user()) ? (Auth::user())->id : false }}}";
        var user_id = $('#user_id').val();
        var twitter_username = $('#twitter_username').val();
        var url = window.location.href
        var arr = url.split("/");
        var host = arr[0] + "//" + arr[2]

        function getHiddenTweets() {
            $.get(host + "/api/user/" + user_id + "/hidden_tweets", function (data) {
                $.each(data, function (key, value) {
                    hidden_tweets.push(value.internal_id);
                })

            }).fail(function () {
                console.error("The request could not be processed.")
            }).done(function () {
                getTweets(hidden_tweets)
            });
        }

        function getTweets(hidden_tweets) {
            TweetJs.ListTweetsOnUserTimeline(twitter_username,
                function (data) {
                    if (data.errors !== undefined) {
                        console.error("The tweets could not be retrieved.")
                        return false;
                    }

                    $.each(data, function (key, tweet) {
                        var is_hidden = $.inArray(tweet.id_str, hidden_tweets) >= 0 ? true : false;

                        if (!logged_in && is_hidden || logged_in && (auth_user != user_id) && is_hidden) {
                            return;
                        }
                        var link = ''
                        if (logged_in && (auth_user == user_id)) {
                            if (is_hidden) {
                                link = '<button type="button" tweet-id="' + tweet.id_str + '" onclick="showTweet(this)" class="btn btn-danger btn-sm show">Show</button>';
                            } else {
                                link = '<button type="button" tweet-id="' + tweet.id_str + '" onclick="hideTweet(this)" class="btn btn-success btn-sm hide">Hide</button>'
                            }
                        }

                        var images = ''
                        $.each(tweet.entities.media, function (key2, value) {
                            images += '<a href="' + value.url + '"><img width="100%" src="' + value.media_url + '"></a>';
                        })
                        $('#tweets').append('<div id="' + tweet.id_str + '" class="card mb-3">' +
                            '<div class="card-body">' +
                            '<h4 class="card-title"><a href="https://twitter.com/' + tweet.user.screen_name + '">' + tweet.user.name + '</a></h4>' +
                            '<h6 class="card-subtitle mb-2 text-muted"><a href="https://twitter.com/' + tweet.user.screen_name + '">@' + tweet.user.screen_name + '</a></h6>' +
                            '<p class="card-text">' + tweet.text + '</p>\n' +
                            images +
                            link +
                            '<a href="https://twitter.com/' + tweet.user.screen_name + '/status/' + tweet.id_str + '" class="card-link float-right">' + tweet.created_at.substring(0, 16) + '</a>' +
                            '</div>' +
                            '</div>')

                    })
                });
        }

        function hideTweet(object) {
            var tweet_id = $(object).attr('tweet-id');
            var settings = {
                "url": host + "/user/" + user_id + "/hide_tweet/" + tweet_id,
                "method": "GET",
                "data": {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                },
                error: function (data, textStatus, xhr) {
                    console.error("The request could not be processed.")
                }
            };

            $.ajax(settings).done(function (response) {
                $('#' + tweet_id + ' button.hide').replaceWith('<button type="button" tweet-id="' + tweet_id + '" onclick="showTweet(this)" class="btn btn-danger btn-sm show">Show</button>');
            });
        }

        function showTweet(object) {
            var tweet_id = $(object).attr('tweet-id');

            var settings = {
                "url": host + "/user/" + user_id + "/show_tweet/" + tweet_id,
                "method": "GET",
                "data": {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "tweet_id": tweet_id,
                },
                error: function (data, textStatus, xhr) {
                    console.error("The request could not be processed.")
                }
            };

            $.ajax(settings).done(function (response) {
                $('#' + tweet_id + ' button.show')
                    .replaceWith('<button type="button" tweet-id="' + tweet_id + '" onclick="hideTweet(this)" class="btn btn-success btn-sm hide">Hide</button>');
            });
        }
    </script>
@endpush
