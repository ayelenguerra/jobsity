@extends('admin.layouts.app')

@section('title', app_name() . ' | ' . __('labels.backend.access.posts.management'))

@section('breadcrumb-links')
    @include('admin.post.includes.breadcrumb-links')
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        {{ __('labels.backend.access.posts.management') }} <small
                            class="text-muted">{{ __('labels.backend.access.posts.all') }}</small>
                    </h4>
                </div><!--col-->

                <div class="col-sm-7">
                    @include('admin.post.includes.header-buttons')
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4">
                <div class="col">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>@lang('labels.backend.access.posts.table.title')</th>
                                <th>@lang('labels.backend.access.posts.table.content')</th>
                                <th>@lang('labels.backend.access.posts.table.created')</th>
                                <th>@lang('labels.general.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{ $post->title }}</td>
                                    <td>{{ \Illuminate\Support\Str::words($post->content, 5, ' ...') }}</td>
                                    <td>{{ $post->created_at  }}</td>
                                    <td class="btn-td">@include('admin.post.includes.actions', ['post' => $post])</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!--col-->
            </div><!--row-->
            <div class="row">
                <div class="col-7">
                    <div class="float-left">
                        {!! $posts->total() !!} {{ trans_choice('labels.backend.access.posts.table.total', $posts->total()) }}
                    </div>
                </div><!--col-->

                <div class="col-5">
                    <div class="float-right">
                        {!! $posts->render() !!}
                    </div>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->
    </div><!--card-->
@endsection
