<div class="col">
    <div class="table">
        <table class="table table-hover">
            <tr>
                <th>@lang('labels.backend.access.posts.table.title')</th>
                <td>{{ $post->title }}</td>
            </tr>

            <tr>
                <th>@lang('labels.backend.access.posts.table.content')</th>
                <td>{{ $post->content }}</td>
            </tr>
        </table>
    </div>
</div><!--table-responsive-->
