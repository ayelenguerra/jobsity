@extends('admin.layouts.app')

@section('title', __('labels.backend.access.posts.management') . ' | ' . __('labels.backend.access.posts.view'))

@section('breadcrumb-links')
    @include('admin.post.includes.breadcrumb-links')
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.posts.management')
                        <small class="text-muted">@lang('labels.backend.access.posts.view')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <div class="row mt-4 mb-4">
                <div class="col">
                    @include('admin.post.show.tabs.overview')
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    <small class="float-right text-muted">
                        <strong>@lang('labels.backend.access.posts.tabs.content.overview.created_at')
                            :</strong> {{ timezone()->convertToLocal($post->created_at) }}
                        ({{ $post->created_at->diffForHumans() }}),
                        <strong>@lang('labels.backend.access.posts.tabs.content.overview.last_updated')
                            :</strong> {{ timezone()->convertToLocal($post->updated_at) }}
                        ({{ $post->updated_at->diffForHumans() }})
                    </small>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
@endsection
