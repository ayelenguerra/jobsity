@extends('admin.layouts.app')

@section('title', __('labels.backend.access.posts.management') . ' | ' . __('labels.backend.access.posts.edit'))

@section('breadcrumb-links')
    @include('admin.post.includes.breadcrumb-links')
@endsection

@section('content')
    {{ html()->modelForm($post, 'PATCH', route('admin.post.update', $post->id))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.posts.management')
                        <small class="text-muted">@lang('labels.backend.access.posts.edit')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(__('strings.backend.title'))->class('col-md-2 form-control-label')->for('title') }}

                        <div class="col-md-10">
                            {{ html()->text('title')
                                ->class('form-control')
                                ->placeholder(__('strings.backend.title'))
                                ->attribute('maxlength', 191)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('strings.backend.content'))->class('col-md-2 form-control-label')->for('content') }}

                        <div class="col-md-10">
                            {{ html()->textarea('content')
                                ->class('form-control')
                                ->placeholder(__('strings.backend.content'))
                                ->attribute('rows', 20)
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.post.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    <a id="button-send" class="btn btn-success btn-sm pull-righ" href="#" role="button">
                        {{__('buttons.general.crud.update')}}
                    </a>
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->closeModelForm() }}
@endsection
@push('after-scripts')
    <script>
        $("form").trigger("reset");
        var arr = (window.location.href).split("/");
        var host = arr[0] + "//" + arr[2]

        $("#button-send").click(function (e) {
            e.preventDefault();
            var settings = {
                "url": $("form").attr('action'),
                "method": "PATCH",
                "data": $('form').serialize(),
                success: function (data, textStatus, xhr) {
                    showAlert('success', 'The post has been saved', '', 1500);
                    $("form").trigger("reset");
                    window.location = host + "/admin/post";
                },
                error: function (data, textStatus, xhr) {
                    showAlert('error', 'The post could not be been saved', getErrorMessage(data));
                }
            };

            $.ajax(settings);
        });
    </script>
@endpush
