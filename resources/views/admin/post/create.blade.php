@extends('admin.layouts.app')

@section('title', __('labels.backend.access.users.management') . ' | ' . __('labels.backend.access.users.create'))

@section('breadcrumb-links')
    @include('admin.post.includes.breadcrumb-links')
@endsection

@section('content')
    {{ html()->form('POST', route('admin.post.store'))->class('form-horizontal')->open() }}
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        @lang('labels.backend.access.posts.management')
                        <small class="text-muted">@lang('labels.backend.access.posts.create')</small>
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row mt-4 mb-4">
                <div class="col">
                    <div class="form-group row">
                        {{ html()->label(__('strings.backend.title'))->class('col-md-2 form-control-label')->for('title') }}

                        <div class="col-md-10">
                            {{ html()->text('title')
                                ->class('form-control')
                                ->attribute('maxlength', 191)
                                ->placeholder(__('strings.backend.title'))
                                ->required()
                                ->autofocus() }}
                        </div><!--col-->
                    </div><!--form-group-->

                    <div class="form-group row">
                        {{ html()->label(__('strings.backend.content'))->class('col-md-2 form-control-label')->for('content') }}

                        <div class="col-md-10">
                            {{ html()->textarea('content')
                                ->attribute('rows', 20)
                                ->class('form-control')
                                ->placeholder(__('strings.backend.content'))
                                ->required() }}
                        </div><!--col-->
                    </div><!--form-group-->
                </div><!--col-->
            </div><!--row-->
        </div><!--card-body-->

        <div class="card-footer clearfix">
            <div class="row">
                <div class="col">
                    {{ form_cancel(route('admin.post.index'), __('buttons.general.cancel')) }}
                </div><!--col-->

                <div class="col text-right">
                    <a id="button-send" class="btn btn-success btn-sm pull-righ" href="#" role="button">
                        {{__('buttons.general.crud.create')}}
                    </a>
                </div><!--col-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
    {{ html()->form()->close() }}
@endsection

@push('after-scripts')
    <script>
        $("form").trigger("reset");
        var arr = (window.location.href).split("/");
        var host = arr[0] + "//" + arr[2]

        $("#button-send").click(function (e) {
            e.preventDefault();
            var settings = {
                "url": $("form").attr('action'),
                "method": "POST",
                "data": $('form').serialize(),
                success: function (data, textStatus, xhr) {
                    showAlert('success', 'The post has been saved', '', 1500);
                    $("form").trigger("reset");
                    window.location = host + "/admin/post";
                },
                error: function (data, textStatus, xhr) {
                    showAlert('error', 'The post could not be been saved', getErrorMessage(data));
                }
            };

            $.ajax(settings);
        });
    </script>
@endpush
