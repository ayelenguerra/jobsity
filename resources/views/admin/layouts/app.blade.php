<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Jobsity Challenge')">
    <meta name="author" content="@yield('meta_author', 'Ayelen Guerra')">
    @yield('meta')

    @stack('before-styles')

    {{ style(mix('css/backend.css')) }}

    @stack('after-styles')
</head>
<body class="app header-fixed sidebar-fixed aside-menu-off-canvas sidebar-lg-show">
@include('admin.includes.header')

<div class="app-body">
    @include('admin.includes.sidebar')

    <main class="main">
        {!! Breadcrumbs::render() !!}

        <div class="container-fluid">
            <div class="animated fadeIn">
                <div class="content-header">
                    @yield('page-header')
                </div><!--content-header-->

{{--                @include('includes.partials.messages')--}}
                @yield('content')
            </div><!--animated-->
        </div><!--container-fluid-->
    </main><!--main-->
</div><!--app-body-->

@include('admin.includes.footer')

<!-- Scripts -->

{{--{!! script(mix('js/app.js')) !!}--}}
@stack('before-scripts')
{!! script(mix('js/manifest.js')) !!}
{!! script(mix('js/vendor.js')) !!}
{!! script(mix('js/backend.js')) !!}
@stack('after-scripts')
</body>
</html>
<script>
    function getErrorMessage(data) {
        var message = '<p class="text-danger">' + data.responseText + '</p>';
        if (data.responseJSON !== undefined) {
            message = '';
            if (data.responseJSON.errors !== undefined) {
                $.each(data.responseJSON.errors, function (key, field) {
                    $.each(field, function (key2, error) {
                        message += '<p class="text-danger">' + error + '</p>'
                    })
                })
            } else {
                $.each(data.responseJSON, function (key, field) {
                    $.each(field, function (key2, error) {
                        message += '<p class="text-danger">' + error + '</p>'
                    })
                })
            }
        }
        return message;
    }

    function showAlert(type, title, html, timer) {
        if (timer !== undefined) {
            Swal.fire({
                position: 'center',
                icon: type,
                title: title,
                showConfirmButton: false,
                html: html,
                timer: timer
            })
        } else {
            Swal.fire({
                position: 'center',
                icon: type,
                title: title,
                showConfirmButton: false,
                html: html,
            })
        }
    }
</script>
