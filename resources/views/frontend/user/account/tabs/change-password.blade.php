{{ html()->form('PATCH', route('frontend.auth.password.update'))->id('change-password-form')->class('form-horizontal')->open() }}
<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.old_password'))->for('old_password') }}

            {{ html()->password('old_password')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.old_password'))
                ->autofocus()
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

            {{ html()->password('password')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.password'))
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}

            {{ html()->password('password_confirmation')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group mb-0 clearfix">
            <a id="change-password-button-send" class="btn btn-success btn-sm pull-righ" href="#" role="button">
                {{__('labels.general.buttons.update') . ' ' . __('validation.attributes.frontend.password')}}
            </a>
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->
{{ html()->form()->close() }}

@push('after-scripts')
    <script>
        $("#change-password-form").trigger("reset");

        $("#change-password-button-send").click(function (e) {
            e.preventDefault();
            var settings = {
                "url": $("#change-password-form").attr('action'),
                "method": $("#change-password-form").attr('method'),
                "data": $('#change-password-form').serialize(),
                success: function (data, textStatus, xhr) {
                    $("#change-password-form").trigger("reset");
                    $("#change-password-button-send").html("Update Password");
                    showAlert('success', data, '', 1500);
                },
                error: function (data, textStatus, xhr) {
                    $("#change-password-button-send").html("Update Password");
                    showAlert('error', 'The password was not updated.', getErrorMessage(data));
                }
            };
            if (validatePasswordForm()) {
                $("#change-password-button-send").html("Sending...");
                $.ajax(settings)
            }
        });

        function validatePasswordForm() {
            $(".invalid-feedback").remove();
            var old_password = $('#old_password').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            var errors = 0

            if (old_password.length < 1) {
                $('#old_password').after('<div class="invalid-feedback">This field is required</div>');
                errors++
            }
            if (password.length < 8) {
                $('#password').after('<div class="invalid-feedback">Password must be at least 8 characters long</div>');
                errors++
            }
            if (password != password_confirmation) {
                $('#password_confirmation').after('<div class="invalid-feedback">The confirmation does not match with the password</div>');
                errors++
            }

            return errors > 0 ? false : true;
        }
    </script>
@endpush
