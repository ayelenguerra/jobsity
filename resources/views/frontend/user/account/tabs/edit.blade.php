{{ html()->modelForm($logged_in_user, 'POST', route('frontend.user.profile.update'))->id('update-user-form')->class('form-horizontal')->attribute('enctype', 'multipart/form-data')->open() }}
@method('PATCH')

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}

            {{ html()->text('first_name')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.first_name'))
                ->attribute('maxlength', 191)
                ->required()
                ->autofocus() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}

            {{ html()->text('last_name')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.last_name'))
                ->attribute('maxlength', 191)
                ->required() }}
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->

<div class="row">
    <div class="col">
        <div class="form-group">
            {{ html()->label(__('validation.attributes.frontend.twitter_username'))->for('twitter_username') }}

            {{ html()->text('twitter_username')
                ->class('form-control')
                ->placeholder(__('validation.attributes.frontend.twitter_username'))
                ->attribute('maxlength', 191)
                ->required()}}
        </div><!--col-->
    </div><!--col-->
</div><!--row-->

@if ($logged_in_user->canChangeEmail())
    <div class="row">
        <div class="col">
            <div class="alert alert-info">
                <i class="fas fa-info-circle"></i> @lang('strings.frontend.user.change_email_notice')
            </div>

            <div class="form-group">
                {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                {{ html()->email('email')
                    ->class('form-control')
                    ->placeholder(__('validation.attributes.frontend.email'))
                    ->attribute('maxlength', 191)
                    ->required() }}
            </div><!--form-group-->
        </div><!--col-->
    </div><!--row-->
@endif

<div class="row">
    <div class="col">
        <div class="form-group mb-0 clearfix">
            <a id="update-user-button-send" class="btn btn-success btn-sm pull-righ" href="#" role="button">
                {{__('labels.general.buttons.update')}}
            </a>
        </div><!--form-group-->
    </div><!--col-->
</div><!--row-->
{{ html()->closeModelForm() }}


@push('after-scripts')
    <script>
        $("#update-user-form").trigger("reset");

        $("#update-user-button-send").click(function (e) {
            e.preventDefault();
            var settings = {
                "url": $("#update-user-form").attr('action'),
                "method": $("#update-user-form").attr('method'),
                "data": $('#update-user-form').serialize(),
                success: function (data, textStatus, xhr) {
                    $("#update-user-button-send").html("Update");
                    showAlert('success', data, '', 1500);
                },
                error: function (data, textStatus, xhr) {
                    $("#update-user-button-send").html("Update");
                    showAlert('error', 'The profile was not updated.', getErrorMessage(data));
                }
            };
            if (validateProfileForm()) {
                $("#update-user-button-send").html("Sending...");
                $.ajax(settings)
            }
        });

        function validateProfileForm() {
            $(".invalid-feedback").remove();
            var first_name = $('#first_name').val();
            var last_name = $('#last_name').val();
            var twitter_username = $('#twitter_username').val();
            var errors = 0
            var required = '<div class="invalid-feedback">This field is required</div>'

            if (first_name.length < 1) {
                $('#first_name').after(required);
                errors++
            }
            if (last_name.length < 1) {
                $('#last_name').after(required);
                errors++
            }
            if (twitter_username.length < 1) {
                $('#twitter_username').after(required);
                errors++
            }

            return errors > 0 ? false : true;
        }
    </script>
@endpush
