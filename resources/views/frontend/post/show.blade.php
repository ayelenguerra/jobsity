@extends('layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')
    <div class="row mb-4">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    {{ $post->title }}
                </div>
                <div class="card-body">
                    {{ $post->content }}
                </div>
                <div class="card-footer">
                    <a href="{{ route('frontend.user.post.show', $post->author) }}" data-toggle="tooltip"
                       data-placement="top">
                        {{ $post->author->username }}
                    </a>
                </div>
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
