<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Jobsity Challenge')">
    <meta name="author" content="@yield('meta_author', 'Ayelen Guerra')">
    @yield('meta')

    @stack('before-styles')

    {{ style(mix('css/frontend.css')) }}

    @stack('after-styles')
</head>
<body>
<div id="app">
    @include('frontend.includes.nav')

    <div class="container">
{{--        @include('includes.partials.messages')--}}
        @yield('content')
    </div><!-- container -->
</div><!-- #app -->

<!-- Scripts -->
@stack('before-scripts')
{!! script(mix('js/manifest.js')) !!}
{!! script(mix('js/vendor.js')) !!}
{!! script(mix('js/frontend.js')) !!}
@stack('after-scripts')

</body>
</html>
<script>
    function getErrorMessage(data) {
        var message = '<p class="text-danger">' + data.responseText + '</p>';
        if (data.responseJSON !== undefined) {
            message = '';
            if (data.responseJSON.errors !== undefined) {
                $.each(data.responseJSON.errors, function (key, field) {
                    $.each(field, function (key2, error) {
                        message += '<p class="text-danger">' + error + '</p>'
                    })
                })
            } else {
                $.each(data.responseJSON, function (key, field) {
                    $.each(field, function (key2, error) {
                        message += '<p class="text-danger">' + error + '</p>'
                    })
                })
            }
        }
        return message;
    }

    function showAlert(type, title, html, timer) {
        if (timer !== undefined) {
            Swal.fire({
                position: 'center',
                icon: type,
                title: title,
                showConfirmButton: false,
                html: html,
                timer: timer
            })
        } else {
            Swal.fire({
                position: 'center',
                icon: type,
                title: title,
                showConfirmButton: false,
                html: html,
            })
        }
    }
</script>
