@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.register_box_title'))

@section('content')
    <div class="row justify-content-center align-items-center">
        <div class="col col-sm-8 align-self-center">
            <div class="card">
                <div class="card-header">
                    <strong>
                        @lang('labels.frontend.auth.register_box_title')
                    </strong>
                </div><!--card-header-->

                <div class="card-body">
                    {{ html()->form('POST', route('frontend.auth.register.post'))->id('register-form')->open() }}
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.first_name'))->for('first_name') }}

                                {{ html()->text('first_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.first_name'))
                                    ->attribute('maxlength', 191)
                                    ->required()}}
                            </div><!--col-->
                        </div><!--row-->

                        <div class="col-12 col-md-6">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.last_name'))->for('last_name') }}

                                {{ html()->text('last_name')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.last_name'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.username'))->for('username') }}

                                {{ html()->text('username')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.username'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.email'))->for('email') }}

                                {{ html()->email('email')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.email'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                                {{ html()->password('password')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.password'))
                                    ->required() }}
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.password_confirmation'))->for('password_confirmation') }}

                                {{ html()->password('password_confirmation')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.password_confirmation'))
                                    ->required() }}
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.twitter_username'))->for('twitter_username') }}

                                {{ html()->text('twitter_username')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.twitter_username'))
                                    ->attribute('maxlength', 191)
                                    ->required()}}
                            </div><!--col-->
                        </div><!--row-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group mb-0 clearfix">
                                <a id="button-send" class="btn btn-success btn-sm pull-righ" href="#" role="button">
                                    {{__('labels.frontend.auth.register_button')}}
                                </a>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                    {{ html()->form()->close() }}

                </div><!-- card-body -->
            </div><!-- card -->
        </div><!-- col-md-8 -->
    </div><!-- row -->
@endsection
@push('after-scripts')
    <script>
        $("form").trigger("reset");
        var arr = (window.location.href).split("/");
        var host = arr[0] + "//" + arr[2]

        $("#button-send").click(function (e) {
            e.preventDefault();

            var settings = {
                "url": $("form").attr('action'),
                "method": $("form").attr('method'),
                "data": $('form').serialize(),
                success: function(data, textStatus, xhr) {
                    $("#button-send").html( "Register");
                    showAlert('success', 'The user was registered.', '', 1500);
                    $("form").trigger("reset");
                    window.location = host;
                },
                error: function(data, textStatus, xhr) {
                    $("#button-send").html( "Register");
                    showAlert('error', data.responseJSON.message, getErrorMessage(data));
                }
            };

            if (validateForm()){
                $("#button-send").html( "Sending...");
                $.ajax(settings)
            }
        });

        function validateForm(){
            var first_name = $('#first_name').val();
            var last_name = $('#last_name').val();
            var email = $('#email').val();
            var password = $('#password').val();
            var password_confirmation = $('#password_confirmation').val();
            var username = $('#username').val();
            var twitter_username = $('#twitter_username').val();

            $(".invalid-feedback").remove();
            var errors = 0

            var required = '<div class="invalid-feedback">This field is required</div>'
            if (first_name.length < 1) {
                $('#first_name').after(required);
                errors++
            }
            if (username.length < 1) {
                $('#username').after(required);
                errors++
            }
            if (twitter_username.length < 1) {
                $('#twitter_username').after(required);
                errors++
            }
            if (last_name.length < 1) {
                $('#last_name').after(required);
                errors++
            }
            if (email.length < 1) {
                $('#email').after(required);
                errors++
            } else {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var validEmail = re.test(email);
                if (!validEmail) {
                    $('#email').after('<div class="invalid-feedback">Enter a valid email</div>');
                    errors++
                }
            }
            if (password.length < 8) {
                $('#password').after('<div class="invalid-feedback">Password must be at least 8 characters long</div>');
                errors++
            }
            if (password != password_confirmation) {
                $('#password_confirmation').after('<div class="invalid-feedback">The confirmation does not match with the password</div>');
                errors++
            }

            return errors > 0 ? false : true;
        }

        function validateEmail(email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            var validEmail = re.test(email);
            // console.log(validEmail)
            return validEmail
        }
    </script>
@endpush

