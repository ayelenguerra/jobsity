@extends('frontend.layouts.app')

@section('title', app_name() . ' | ' . __('labels.frontend.auth.login_box_title'))

@section('content')
    <div class="row justify-content-center align-items-center">
        <div class="col col-sm-8 align-self-center">
            <div class="card">
                <div class="card-header">
                    <strong>
                        @lang('labels.frontend.auth.login_box_title')
                    </strong>
                </div><!--card-header-->

                <div class="card-body">
                    {{ html()->form('POST', route('frontend.auth.login.post'))->id('login-form')->open() }}
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.username'))->for('username') }}

                                {{ html()->text('username')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.username'))
                                    ->attribute('maxlength', 191)
                                    ->required() }}
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                {{ html()->label(__('validation.attributes.frontend.password'))->for('password') }}

                                {{ html()->password('password')
                                    ->class('form-control')
                                    ->placeholder(__('validation.attributes.frontend.password'))
                                    ->required() }}
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <div class="checkbox">
                                    {{ html()->label(html()->checkbox('remember', true, 1) . ' ' . __('labels.frontend.auth.remember_me'))->for('remember') }}
                                </div>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->

                    <div class="row">
                        <div class="col">
                            <div class="form-group clearfix">
                                <a id="button-send" class="btn btn-success btn-sm pull-righ" href="#" role="button">
                                    {{__('labels.frontend.auth.login_button')}}
                                </a>
                            </div><!--form-group-->
                        </div><!--col-->
                    </div><!--row-->
                    {{ html()->form()->close() }}

                </div><!--card body-->
            </div><!--card-->
        </div><!-- col-md-8 -->
    </div><!-- row -->
@endsection

@push('after-scripts')
    <script>
        $("form").trigger("reset");
        var arr = (window.location.href).split("/");
        var host = arr[0] + "//" + arr[2]

        $("#button-send").click(function (e) {
            e.preventDefault();
            var settings = {
                "url": $("form").attr('action'),
                "method": $("form").attr('method'),
                "data": $('form').serialize(),
                success: function (data, textStatus, xhr) {
                    window.location = host;
                },
                error: function (data, textStatus, xhr) {
                    console.log(data)
                    $("#button-send").html("Login");
                    showAlert('error', data.responseJSON.message, getErrorMessage(data));
                }
            };
            if (validateForm()) {
                $("#button-send").html("Sending...");
                $.ajax(settings)
            }
        });

        function validateForm() {
            $(".invalid-feedback").remove();
            var password = $('#password').val();
            var username = $('#username').val();
            var errors = 0
            var required = '<div class="invalid-feedback">This field is required</div>'

            if (username.length < 1) {
                $('#username').after(required);
                errors++
            }
            if (password.length < 1) {
                $('#password').after(required);
                errors++
            }

            return errors > 0 ? false : true;
        }
    </script>
@endpush
