<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Jobsity challenge')">
    <meta name="author" content="@yield('meta_author', 'Ayelen Guerra')">
@yield('meta')

@stack('before-styles')
{{ style(mix('css/frontend.css')) }}
    @stack('after-styles')
</head>
<body>
<div id="app">
    @include('includes.nav')
    <div class="container">
{{--        @include('includes.partials.messages')--}}
        @yield('content')
    </div><!-- container -->
</div><!-- #app -->

@include('includes.partials.footer')
<!-- Scripts -->
@stack('before-scripts')
{!! script(mix('js/manifest.js')) !!}
{!! script(mix('js/vendor.js')) !!}
{!! script(mix('js/frontend.js')) !!}
@stack('after-scripts')
</body>
</html>
