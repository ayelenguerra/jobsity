@extends('layouts.app')

@section('title', app_name() . ' | ' . __('navs.general.home'))

@section('content')

    @foreach($posts as $post)
        <div class="row mb-4">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ route('frontend.post.show', $post) }}" data-toggle="tooltip" data-placement="top"
                           title="@lang('buttons.general.crud.view')">
                            {{ $post->title }}
                        </a>

                        @if (Auth::check() && Auth::user()->id == $post->author->id)

                            <div class="btn-group float-right" role="group"
                                 aria-label="@lang('labels.backend.access.posts.post_actions')">
                                <a href="{{ route('admin.post.edit', $post) }}" data-toggle="tooltip"
                                   data-placement="top"
                                   title="@lang('buttons.general.crud.edit')" class="btn btn-primary">
                                    <i class="fas fa-edit"></i>
                                </a>
                            </div>

                        @endif
                    </div>
                    <div class="card-body">
                        {{ \Illuminate\Support\Str::words($post->content, 50, ' ...') }}
                        <a href="{{ route('frontend.post.show', $post) }}" data-toggle="tooltip" data-placement="top">
                            (see more)
                        </a>
                    </div>
                    <div class="card-footer float-right">
                        <div class="float-right">
                            <a href="{{ route('frontend.user.post.show', $post->author) }}" data-toggle="tooltip"
                           data-placement="top">
                            {{ $post->author->username }}
                            </a>
                        </div>
                    </div>
                </div><!--card-->
            </div><!--col-->
        </div><!--row-->
    @endforeach


    <div class="text-xs-center">
        {{ $posts->links() }}

    </div>
@endsection
