require('./bootstrap');

function getErrorMessage(data) {
    var message = '<p class="text-danger">' + data.responseText + '</p>';
    if (data.responseJSON !== undefined) {
        message = '';
        if (data.responseJSON.errors !== undefined) {
            $.each(data.responseJSON.errors, function (key, field) {
                $.each(field, function (key2, error) {
                    message += '<p class="text-danger">' + error + '</p>'
                })
            })
        } else {
            $.each(data.responseJSON, function (key, field) {
                $.each(field, function (key2, error) {
                    message += '<p class="text-danger">' + error + '</p>'
                })
            })
        }
    }
    console.log(data.responseJSON)
    console.log(data)
    console.log(message)
    return message;
}

function showAlert(type, title, html, timer) {
    if (timer !== undefined) {
        Swal.fire({
            position: 'center',
            icon: type,
            title: title,
            showConfirmButton: false,
            html: html,
            timer: timer
        })
    } else {
        Swal.fire({
            position: 'center',
            icon: type,
            title: title,
            showConfirmButton: false,
            html: html,
        })
    }
}
