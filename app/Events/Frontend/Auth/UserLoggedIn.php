<?php

namespace App\Events\Frontend\Auth;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

//use App\Models\Auth\User;

/**
 * Class UserLoggedIn.
 */
class UserLoggedIn
{
    use SerializesModels;

    /**
     * @var
     */
    public $user;

    /**
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
