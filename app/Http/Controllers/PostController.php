<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Auth::user()->posts()->orderBy('created_at', 'desc')->paginate(5);

        return view('admin.post.index', compact('posts'))->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.post.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return response()->json($errors, 400);
        }

        $post = new Post();
        $post->fill($request->all());
        $post->author_id = Auth::user()->id;
        try {
            $post->save();
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return response($post, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('admin.post.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        return view('admin.post.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required',
            'content' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->messages()->get('*');
            return response()->json($errors, 400);
        }

        try {
            $post->update($request->all());
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return response($post, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();

        return redirect()->route('admin.post.index')
            ->with('success', 'Post deleted successfully');
    }

    /**
     * @param ManagePostRequest $request
     * @param Post $deletedPost
     *
     * @return mixed
     * @throws \Throwable
     * @throws \App\Exceptions\GeneralException
     */
    public function delete(ManagePostRequest $request, Post $deletedPost)
    {
        $deletedPost->forceDelete();

        return redirect()->route('admin.post.index')->withFlashSuccess(__('alerts.backend.posts.deleted_permanently'));
    }

    /**
     * @param ManagePostRequest $request
     * @param Post $deletedPost
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function restore(ManagePostRequest $request, Post $deletedPost)
    {
        $this->postRepository->restore($deletedPost);

        return redirect()->route('admin.post.index')->withFlashSuccess(__('alerts.backend.posts.restored'));
    }
}
