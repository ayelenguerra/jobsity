<?php

namespace App\Http\Controllers\Frontend\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\User\UpdateProfileRequest;
use App\Repositories\Frontend\Auth\UserRepository;

/**
 * Class ProfileController.
 */
class ProfileController extends Controller // todo: refactor
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * ProfileController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return mixed
     * @throws \App\Exceptions\GeneralException
     */
    public function update(UpdateProfileRequest $request)
    {
        try {
            $this->userRepository->update(
                $request->user()->id,
                $request->only('username', 'first_name', 'last_name', 'email', 'twitter_username'), false
            );
        } catch (\Exception $e) {
            return response($e->getMessage(), 400);
        }

        return response(__('strings.frontend.user.profile_updated'), 200);
    }
}
