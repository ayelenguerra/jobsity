<?php

namespace App\Http\Controllers;

use App\Models\Tweet;
use Illuminate\Support\Facades\Auth;

class TweetController extends Controller
{

    public function hideTweet(int $user_id, string $tweet_id)
    {
        if (Auth::user()->id !== $user_id) {
            return response()->json('The logged user is not allowed to this action.', 400);
        }

        $data = [
            'internal_id' => $tweet_id,
            'user_id' => Auth::id()
        ];
        Tweet::create($data);

        return response()->json('success', 200);
    }

    public function showTweet(int $user_id, string $tweet_id)
    {
        if (Auth::user()->id !== $user_id) {
            return response()->json('The logged user is not allowed to this action.', 400);
        }

        $tweet = Tweet::where('internal_id', $tweet_id)->first();
        $tweet->delete();

        return response()->json('success', 200);
    }
}
