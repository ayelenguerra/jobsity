<?php

namespace App\Http\Controllers\Backend\Auth\User;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Repositories\Backend\Auth\UserRepository;

/**
 * Class UserController.
 */
class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getPosts($user_id)
    {
        $user = User::findOrFail($user_id);
        $posts = $user->posts()->paginate(3);

        return view('user.post.index', ['posts' => $posts, 'author' => $user])->with('i', (request()->input('page', 1) - 1) * 5);
    }

    public function getHiddenTweets($user_id)
    {
        $user = User::findOrFail($user_id);
        $tweets = $user->tweets()->get();

        return response()->json($tweets, 200);
    }
}
