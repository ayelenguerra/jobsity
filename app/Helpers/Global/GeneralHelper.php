<?php

use App\Helpers\General\TimezoneHelper;

if (!function_exists('app_name')) {
    /**
     * Helper to grab the application name.
     *
     * @return mixed
     */
    function app_name()
    {
        return config('app.name');
    }

    if (!function_exists('index_route')) {
        /**
         * Return the route to the "home" page depending on authentication/authorization status.
         *
         * @return string
         */
        function index_route()
        {
            return 'index'; // todo : refactor
//            if (auth()->check()) {
//                if (auth()->user()->can('view backend')) {
//                    return 'admin.dashboard';
//                }
//
//                return 'frontend.user.dashboard';
//            }
//
//            return 'frontend.index';
        }
    }

    if (!function_exists('home_route')) {
        /**
         * Return the route to the "home" page depending on authentication/authorization status.
         *
         * @return string
         */
        function home_route()
        {
            return 'index';
            if (auth()->check()) {
                if (auth()->user()->can('view backend')) {
                    return 'admin.dashboard';
                }

                return 'frontend.user.dashboard';
            }

            return 'frontend.index';
        }
    }

    if (!function_exists('timezone')) {
        /**
         * Access the timezone helper.
         */
        function timezone()
        {
            return resolve(TimezoneHelper::class);
        }
    }

}

