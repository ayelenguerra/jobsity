<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 1)->create([
            'username' => 'john.doe',
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'john.doe@jobsity.com',
            'twitter_username' => 'johndoe_',
        ]);

        factory(User::class, 1)->create([
            'username' => 'daenerys',
            'first_name' => 'Daenerys',
            'last_name' => 'Targaryen',
            'email' => 'daenerys.targaryen@gmail.com',
            'twitter_username' => 'DaenerysSpain',
        ]);

        factory(User::class, 1)->create([
            'username' => 'arya.stark',
            'first_name' => 'Arya',
            'last_name' => 'Stark',
            'email' => 'arya.stark@gmail.com',
            'twitter_username' => 'AryaOfWinterfel',
        ]);

        factory(User::class, 1)->create([
            'username' => 'tommy.shelby',
            'first_name' => 'Tommy',
            'last_name' => 'Shelby',
            'email' => 'tommy.shelby@gmail.com',
            'twitter_username' => '__ThomasShelby',
        ]);
    }
}
