<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;

$factory->define(Post::class, function (Faker $faker) {
    return [
        'title' => $faker->numerify('Title ###'),
        'content' => $faker->realText(1500),
        'author_id' => $faker->randomElement(\App\Models\User::all()->pluck('id')->toArray()),
    ];
});
