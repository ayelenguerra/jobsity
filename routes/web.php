<?php

use App\Http\Controllers\Backend\Auth\User\UserController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Frontend\Auth\LoginController;
use App\Http\Controllers\Frontend\Auth\RegisterController;
use App\Http\Controllers\Frontend\Auth\ResetPasswordController;
use App\Http\Controllers\Frontend\Auth\UpdatePasswordController;
use App\Http\Controllers\Frontend\User\AccountController;
use App\Http\Controllers\Frontend\User\ProfileController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Models\Post;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('index');

/*
 * Frontend Routes
 */
Route::group(['namespace' => 'Frontend', 'as' => 'frontend.'], function () {
    Route::get('/post/{post}', function ($post) {
        return view('frontend.post.show', ['post' => Post::findOrFail($post)]);
    })->name('post.show');

    Route::get('/user/{user}/post', [UserController::class, 'getPosts'])->name('user.post.show');

    Route::group(['namespace' => 'Auth', 'as' => 'auth.'], function () {
        // These routes require no user to be logged in
        Route::group(['middleware' => 'guest'], function () {
            // Authentication Routes
            Route::get('login', [LoginController::class, 'showLoginForm'])->name('login');
            Route::post('login', [LoginController::class, 'login'])->name('login.post');
//            Route::post('login', [LoginController::class, 'login'])->name('login.post');
            // Registration Routes
            Route::get('register', [RegisterController::class, 'showRegistrationForm'])->name('register');
            Route::post('register', [RegisterController::class, 'register'])->name('register.post');

            Route::get('password/reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('password.reset.form');
            Route::post('password/reset', [ResetPasswordController::class, 'reset'])->name('password.reset');
        });

        Route::group(['middleware' => 'auth'], function () {
            Route::get('logout', [LoginController::class, 'logout'])->name('logout');

            // Change Password Routes
            Route::patch('password/update', [UpdatePasswordController::class, 'update'])->name('password.update');
        });
    });

    Route::group(['middleware' => ['auth']], function () {
        Route::get('user/{user_id}/hide_tweet/{tweet_id}', [\App\Http\Controllers\TweetController::class, 'hideTweet'])->name('tweet.hide');
        Route::get('user/{user_id}/show_tweet/{tweet_id}', [\App\Http\Controllers\TweetController::class, 'showTweet'])->name('tweet.show');
        Route::group(['namespace' => 'User', 'as' => 'user.'], function () {
            // User Account Specific
            Route::get('account', [AccountController::class, 'index'])->name('account');

            // User Profile Specific
            Route::patch('profile/update', [ProfileController::class, 'update'])->name('profile.update');
        });
    });
});

/*
 * Admin Routes
 */
Route::group(['namespace' => 'Backend', 'prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    Route::group(['middleware' => 'auth'], function () {

        // Post Management
        Route::group(['namespace' => 'Post'], function () {
            // Post CRUD
            Route::get('post', [PostController::class, 'index'])->name('post.index');
            Route::get('post/create', [PostController::class, 'create'])->name('post.create');
            Route::post('post', [PostController::class, 'store'])->name('post.store');

            // Specific Post
            Route::group(['prefix' => 'post/{post}'], function () {
                // Post
                Route::get('/', [PostController::class, 'show'])->name('post.show');
                Route::get('edit', [PostController::class, 'edit'])->name('post.edit');
                Route::patch('/', [PostController::class, 'update'])->name('post.update');
                Route::delete('/', [PostController::class, 'destroy'])->name('post.destroy');
            });
        });

        // All route names are prefixed with 'admin.'.
        Route::redirect('/', '/admin/dashboard', 301);
        Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');

    });
});

