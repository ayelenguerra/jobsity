<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/


// Home
//Breadcrumbs::for('home', function ($trail) {
//    $trail->push('Home');
//
//});

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

//Breadcrumbs::for('admin.dashboard', function ($trail) {
//    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
//});

Breadcrumbs::for('admin.post.index', function ($trail) {
    $trail->push(__('labels.backend.access.posts.management'), route('admin.post.index'));
});

Breadcrumbs::for('admin.post.create', function ($trail) {
    $trail->parent('admin.post.index');
    $trail->push(__('labels.backend.access.posts.create'), route('admin.post.create'));
});

Breadcrumbs::for('admin.post.show', function ($trail, $id) {
    $trail->parent('admin.post.index');
    $trail->push(__('menus.backend.access.posts.view'), route('admin.post.show', $id));
});

Breadcrumbs::for('admin.post.edit', function ($trail, $id) {
    $trail->parent('admin.post.index');
    $trail->push(__('menus.backend.access.posts.edit'), route('admin.post.edit', $id));
});

