<?php

use Illuminate\Http\Request;
use App\Http\Controllers\TweetController;
use App\Http\Controllers\Backend\Auth\User\UserController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('user/{user_id}/hidden_tweets', [UserController::class, 'getHiddenTweets'])->name('api.user.hidden_tweets');

Route::group(['middleware' => ['auth.basic']], function () {
    Route::get('user/{user_id}/hide_tweet/{tweet_id}', [TweetController::class, 'hideTweet'])->name('tweet.hide');
    Route::get('user/{user_id}/show_tweet/{tweet_id}', [TweetController::class, 'showTweet'])->name('tweet.show');
});
